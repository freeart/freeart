
.. currentmodule:: freeart.configuration.fileInfo

:mod:`fileInfo`: classes to know where to save / load data
----------------------------------------------------------

.. automodule:: freeart.configuration.fileInfo
   :members: