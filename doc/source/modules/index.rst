freeart API
===========

.. toctree::
   :maxdepth: 3

   python_interface/index.rst
   utils/index.rst
   interpreter/index.rst
   configuration/index.rst
