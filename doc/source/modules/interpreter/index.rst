.. currentmodule:: freeart.interpreter

:mod:`freeart.interpreter`
-------------------------

.. automodule:: freeart.interpreter


:mod:`freeart.interpreter.configinterpreter`: config file management
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

.. currentmodule:: freeart.interpreter.configinterpreter
.. automodule:: freeart.interpreter.configinterpreter
    :members:
