\section transmission_projector

From freeART you can produce a sinogram from a phantom by a forward projection.

Here is an example of a c++ code to produce this kind of sinogram : 

\code
    FreeART::AlgorithmIO algoIO;
    // the input phantom
    FreeART::BinVec3D<double> phantom;

    // we will set the geometry from the phantom and the set of angles we are interesting in
    FreeART::SinogramsGeometry sinosGeo;
    algoIO.prepareSinogramGeneration(phantomFileName, MIN_ANGLE, MAX_ANGLE, TOT_PROJECTIONS, phantom, sinosGeo);
    FreeART::SARTAlgorithm<double,FreeART::TxReconstruction>* al = new FreeART::SARTAlgorithm<double,FreeART::TxReconstruction>(phantom,sinosGeo);
    // the we generate the sinogram   
    al->makeSinogram();
    FREEART_NAMESPACE::GenericSinogram3D<double> sinogram = al->getSinogram();

\endcode

\warning For now it seems that for fluorescence projection we obtain different sinograms depending on if we call the constructor without any self absorption matrix or with an other constructor with nul self-absorption matrix.

\section fluorescence_projector

From python 
"""""""""""

.. code-block:: python
    :emphasize-lines: 3,5

    # define reconstruction parameters
    al = freeart.FluoFwdProjection(
        phMatr           = phantom, 
        expSetUp         = detSetup,
        absorpMatr       = absMat,
        selfAbsorpMatrix = selfAbsMat,
        angleList        = None, 
        minAngle         = 0, 
        maxAngle         = 2.0*np.pi, 
        anglesNb         = numAngle )

    al.setOverSampling(oversampling)
    al.setSamplingWithInterpolation(samplingWithInterpolation)
    al.setCreateOneRayPerSamplePt(createOneRayPerSamplePoint)
    # create the sinogram
    sinogram, angles = al.makeSinogram()


.. note:: you can see a more detaillled example in the freeart/python_utils/example_fluo.py file

see more information in the fluorescence section
