\section reconstruction

For each kind of reconstruciton you should give the reconstruction parameters : TODO : add a link to the potion and the sinogram to use to make the reconstruction

\section projection

\subsection transmission

In order to generate a sinogram from a transmission projector you need to give to the library the phantom of the sample containing for each voxel the absorption on this section.

\subsection fluoresence_and_compton fluoresence and compton

The fluorescence projector is caracterized by the fact that the detector is not receiving directly the beam from the X ray source (incoming beam) but an other beam (outgoing beam) which is generated from an interaction between the sample and the incoming beam. The detector will only receive a small part of the beam produced. You can see the scheme bellow which medel the process.

\image html fluorescence_example.png

Those two beam can have the same energy. In this case we are in the COMPTON mode or having two different energies the FLUORESENCE mode.
The formula giving the emission rate of the element i detected is (for one ray) : 


\f$dI(E_F)=I_0.e^{-\frac{{\mu}_{E_0}{\rho}x}{sin({\psi}_1)}}.\frac{{\rho}C_i{\tau}_{is} (E_0)}{sin({\psi_1})}dx.{\omega}_{is} R_{is} (E_F).e^{-\frac{{\mu}_{E_F}{\rho}x}{sin({\psi}_2)}}.\frac{d{\Omega}}{4{\pi}}{\epsilon}_D(E_F, {\Omega})\f$

with : 
<ul>
<li> \f$e^{-\frac{{\mu}_{E_0}{\rho}x}{sin({\psi}_1)}}\f$  : Rate of incident photons at depth x (1)
<li> \f$\frac{{\rho}C_i{\tau}_{is} (E_0)}{sin({\psi_1})}dx\f$ : Probability of production of vacancy in the atomic shell s of the element i in the path 
\f$\frac{dx}{sin{\psi}_1} \f$ (2)
<li> \f${\omega}_{is} R_{is} (E_F)\f$ : Probability of emission of a photon of energy \f$E_F\f$ of the element i among the family of emitted photons corresponding to transitions to the atomic shell (3)
<li> \f$e^{-\frac{{\mu}_{E_F}{\rho}x}{sin({\psi}_2)}}\f$ : Transmission of the fluorescent radiation in the outgoing path towards the detector (4)
<li> \f$\frac{d{\Omega}}{4{\pi}}{\epsilon}_D(E_F, {\Omega})\f$ : overall detection efficiency for \f$E_F\f$ photons (aka solid angle) (5)
</ul>

In FreeART for now \f$I_0=1\f$
Then the geometry (distane that each beam is going trought, solid angle, ...) is deduced.
What you should give to FreeART is :
<ul>
<li> \f$ {\mu}_{E_0}{\rho}\f$ from the "absorption" matrix
<li> \f$ {\mu}_{E_F}{\rho}\f$ from the "self absorption" matrix
<li> probabilty of production of a photon of energy \f$E_F\f$ of the element i for each voxel (including the probability of production of a vacancy in the atomic shell s of the element i) from the "phantom" matrix
</ul>

\subsection convention_matrices_size convention matrices size

<ul>
<li> a sinogram is a 3D matrix with : 
    <ol>
    <li> x = slice index (for now x_size = 1 : we only take "2D" sinograms)
    <li> y = angle (y_size = nb angles )
    <li> z = ray (z_size = nb ray)
    </ol>
    \n
<li> Absorption matrix / Self Absorption matrix are 2D matrix containing for each voxel an absorption coefficient. This is the effect of the particle attenuation throught the sample. Because the number of voxels is dependante on the number of ray we are recording, dimensions of those type of matrix are : 
    <ol>
    <li> x_size = nbRay
    <li> y_size = nbRay
    </ol>
    \n
<li> Phantom of a sample we are scanning
    <ol>
    <li> x_size = voxelX
    <li> y_size = voxelY
    </ol>
    \n
<li> detector set up (dets) : This is detector set up (at least position and width). This is needed for compton fluorescence and diffraction reconstructions and projections.
<li> nb iteration : FreeART stands for Algebraic Reconstruction Technique which is an iterative method. This is simly the number of iteration we want to run.
<li> energies : when we run a fluorescence reconstruction we have sinogrames for a couples (chemical element, energy). This parameter is the list of recorded energies.
<li> sinoAngles : the angles where the recording of the tomography as been made.
</ul>

\section output_data output data

For each type of reconstruction we will generate a 2D matrices which a phantom with absorption coefficient for each voxel we have created.
The number of voxels is dependande on the number of ray we are recording and on the voxel size parameter.
This is whiy for a voxel size of 1 ( default voxel size ) we will have phantom dimensions = (nbRay, nbRay)