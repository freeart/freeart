\section FreeART geometry

FreeART is set on the previous convention :
   - beam's source is setted at (0, 1) for the beam angle 0. Then the beam will be directed by the vector (0, -1) until it reqch the detector (Transmission case for example) or until a second ray is generated (Fluorescence mode for example).
    The beam angle direction is clockwise.
   - Detecor position is set on the same reference. At angle \f$\theta\f$ he will be located at distance*sin(\f$\theta\f$), distance*cos(\f$\theta\f$).
   - unit of absoprtion are giving as 
   \f$\frac{g}{mm^2}\f$
   - The sample is fix, it is the couple detector, incoming beam which are rotating in such a way that the angle betzeen the both of them are constant.

   In the case where we want to process acquisition throught angle 0 degree and 45 degree with the detector set a 180 degree we will obatin the two following setup ( here for transmission and for the ray 'n' ):
   \image html acquisition_theta_0.png "acquisition for angle at 0 degree"
   \image html acquisition_theta_45.png "acquisition for angle at 45 degree"

\section FreeART from python
   - you can access to freeART library from a python bidding.
   Documentation is presented in the python part.