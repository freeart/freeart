\section transmission_reconstruction
in cpp :
\code
    FreeART::SinogramsGeometry sinosGeo; // the complete geometry of the experiment
    FreeART::Sinograms3D<double> sinos;  // data set
    FreeART::AlgorithmIO algoIO;         // object to deal with IO and setting the SART algorithm structure
    FreeART::ExperimentSetUp esu;        // experiment set up

    // here no geometry. No constructor without the Experiment SetUp paramter
    algoIO.buildSinogramGeometry(sinoFile, esu, sinos, sinosGeo);

    FreeART::SARTAlgorithm<double,FreeART::TxReconstruction> *al; // the reconstruction algorithm

    // create the transmission reconstruction
    al = new FreeART::SARTAlgorithm<double,FreeART::TxReconstruction>(sinos,sinosGeo);
    al->doWork(iterNb);

    const FreeART::BinVec3D<double>* v = &(al->getPhantom());
    write_data_to_edf(static_cast<const std::vector<double>*> (v), v->getLength(), v->getWidth(), outFile.c_str());

    delete al;
\endcode
A full example is given in the file : cpp_utils/reconstr_tx.cpp
in python :  
\code
    import freeart 

    nbIter = int(sys.argv[1])

    # get sino data
    sinoData,sinoAngles = reconstr_utils.readSinoFile(sys.argv[2])

    # no detector for this type of projection
    al = freeart.TxBckProjection(sinoData,sinoAngles)

    # FluoBckProjection
    txPhantom = al.iterate(nbIter)

    # save the reconstruction    
    reconstr_utils.savePhantom(txPhantom, sys.argv[3])
\endcode 
A full example is given in the file python_utils/reconstr_tx
\section compton_reconstruction
A comptn reconstruction is the same as a fluorescence reconstruction but with nul absorption and self-absorption matrices.

