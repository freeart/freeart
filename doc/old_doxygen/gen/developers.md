\section developers

\subsection oversampling a word on oversampling

If we set the oversampling value to one then we will generate one point at each VOXEL_WIDTH to get the value at this point.
For fluorescence we need to normalize the sampling of the \b incoming beam because the ray width is 1 and if we take values from more than the RAY_WIDTH == VOXEL_WIDTH then we will end up with more "photons" that we should.
This normalization isn't done for the otgoing beam (with the CREATE_ONE_RAY_PER_SAMPLE_POINT). Because each outgoing ray refer to one sample point.