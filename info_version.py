import silx
print('silx version is ', silx._version.version)
from silx.gui import qt
print('qt version is ', qt.qVersion())
import numpy
print('numpy version is ', numpy.version.version)
import fisx
print('fisx version is ', fisx.version())
